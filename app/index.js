const express = require('express');
const app = express();
const path = require('path');
const ejs = require('ejs');


app.set('view engine', 'ejs');
app.set('views', path.resolve('./resource/views'));

app.get('/', function (req, res) {
    res.render('index');
})

app.listen(3000)